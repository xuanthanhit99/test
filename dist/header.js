"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _Nav = _interopRequireDefault(require("react-bootstrap/Nav"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var header = function header(props) {
  return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement(_Nav["default"], {
    activeKey: "/home",
    onSelect: function onSelect(selectedKey) {
      return alert("selected ".concat(selectedKey));
    }
  }, /*#__PURE__*/_react["default"].createElement(_Nav["default"].Item, null, /*#__PURE__*/_react["default"].createElement(_Nav["default"].Link, {
    href: "/$\u01A0}"
  }, "Active")), /*#__PURE__*/_react["default"].createElement(_Nav["default"].Item, null, /*#__PURE__*/_react["default"].createElement(_Nav["default"].Link, {
    eventKey: "link-1"
  }, "Link")), /*#__PURE__*/_react["default"].createElement(_Nav["default"].Item, null, /*#__PURE__*/_react["default"].createElement(_Nav["default"].Link, {
    eventKey: "link-2"
  }, "Link")), /*#__PURE__*/_react["default"].createElement(_Nav["default"].Item, null, /*#__PURE__*/_react["default"].createElement(_Nav["default"].Link, {
    eventKey: "disabled",
    disabled: true
  }, "Disabled"))));
};

var _default = header;
exports["default"] = _default;